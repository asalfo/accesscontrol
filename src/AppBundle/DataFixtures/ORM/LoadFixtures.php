<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Access;
use AppBundle\Entity\Membership;
use AppBundle\Entity\User;
use AppBundle\Entity\Client;
use AppBundle\Entity\Rate;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the sample data to load in the database when running the unit and
 * functional tests. Execute this command to load the data:
 *
 *   $ php app/console doctrine:fixtures:load
 *
 * See http://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html
 *
 * @author  Salif Guigma <salif.guigma@gmail.com>
 */
class LoadFixtures implements FixtureInterface, ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    public function load(ObjectManager $manager)
    {
        $this->loadUsers($manager);
        $this->loadClients($manager);
    }

    private function loadUsers(ObjectManager $manager)
    {
        $passwordEncoder = $this->container->get('security.password_encoder');

        $user1 = new User();
        $user1->setUsername('recepcion');
        $user1->setEmail('recepcion@accesscontrol.com');
        $user1->setEmail('recepcion@accesscontrol.com');
        $user1->setRoles(array('ROLE_USER'));
        $encodedPassword = $passwordEncoder->encodePassword($user1, 'recepcion');
        $user1->setPassword($encodedPassword);
        $user1->setEnabled(1);
        $manager->persist($user1);

        $adminUser = new User();
        $adminUser->setUsername('admin');
        $adminUser->setEmail('admin@accesscontrol.com');
        $adminUser->setRoles(array('ROLE_ADMIN'));
        $encodedPassword = $passwordEncoder->encodePassword($adminUser, 'admin');
        $adminUser->setPassword($encodedPassword);
        $adminUser->setEnabled(true);
        $manager->persist($adminUser);
        $manager->flush();
    }

    private function loadClients(ObjectManager $manager)
    {
        $validator = $this->container->get('validator');
        $ids = array('Z4853287Q', 'Y3235628W', 'X0331220C', 'Y1774002A', 'Z9316436S', 'Z1463169Y', '40021834V', '01468103J',
            '42105319R', '29521741E', '52460130M', '60002763H', '32851579N', '71084242M', '35111797C', '52303550D');


        $rate0 = new Rate();
        $rate0->setTitle("Normal");
        $rate0->setPrice(4);
        $rate0->setDuration(1);
        $rate0->setStatus(true);
        $manager->persist($rate0);

        $rate0 = new Rate();
        $rate0->setTitle("VIP Semana");
        $rate0->setPrice(8);
        $rate0->setDuration(7);
        $rate0->setStatus(true);
        $manager->persist($rate0);

        $rate0 = new Rate();
        $rate0->setTitle("VIP 1 mes");
        $rate0->setPrice(18);
        $rate0->setDuration(30);
        $rate0->setStatus(true);
        $manager->persist($rate0);

        $rate0 = new Rate();
        $rate0->setTitle("VIP 3 meses");
        $rate0->setPrice(45);
        $rate0->setDuration(90);
        $rate0->setStatus(true);
        $manager->persist($rate0);

        $rate0 = new Rate();
        $rate0->setTitle("VIP 6 meses");
        $rate0->setPrice(60);
        $rate0->setDuration(180);
        $rate0->setStatus(true);
        $manager->persist($rate0);

        foreach ($ids as $index => $id) {
            $client = new Client();
            $client->setIdCardType(1);
            $client->setIdCardNumber($id);
            $cart = str_pad(rand(1,29999), 10, "0", STR_PAD_LEFT);
            $client->setCardNumber($cart);
            $client->setName("nombre_" . $index);
            $client->setSurname("Apellido_" . $index);
            $client->setEmail("email$index@mail.com");
            $client->setState(Client::STATE_ACTIVE);
            $errors = $validator->validate($client);
            if(count($errors) == 0){
                $manager->persist($client);



                $membership = new Membership();
                $membership->setClient($client);
                $membership->setRate($rate0);
                $membership->setStartDate(new \DateTime());
                $membership->setStatus(true);
                $manager->persist($membership);


                $access = new Access();
                $access->setClient($client);
                $access->setLocker(rand(1,100));
                $now = new \DateTime('now');
                $access->setStartDate($now);
                $clone = clone $now;
                $clone->modify('+1 hour');
                $access->setEndDate($clone);
                $manager->persist($access);

                $manager->flush();
            }
        }

    }



    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


}

<?php

namespace AppBundle\Form;

use AppBundle\Entity\Client;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ClientTransformer implements DataTransformerInterface
{


    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }


    /**
     * Transforms an object (client) to a string (number).
     * @param mixed $client
     * @return string
     */
    public function transform($client)
    {
        if(null == $client){
            return '';
        }

        return $client->getId();
    }

    /**
     * Transforms a string (number) to an object (client).
     * @param mixed $clientId
     * @return object|void
     */
    public function reverseTransform($clientId)
    {

        if (!$clientId) {
            return;
        }

        $client= $this->manager
            ->getRepository('AppBundle:Client')
            ->find($clientId)
        ;

        if (null === $client) {
            throw new TransformationFailedException(sprintf(
                'A client with id "%s" does not exist!',
                $clientId
            ));
        }

        return $client;
    }


}
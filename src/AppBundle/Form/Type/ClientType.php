<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idCardType','choice', array(
                'label' => 'Tipo documento',
                'choices'  => array(1 => 'DNI','2' =>'Tarjeta de Residencia','3' =>'Pasaporte'),
                'required' => true))
            ->add('idCardNumber','text', array('label' => 'Nº documento'))
            ->add('cardNumber',"text",array('label' => 'Nº Socio'))
            ->add('name','text',array('label' => 'Nombre'))
            ->add('surname','text',array('label' => 'Apellidos'))
            ->add('email','email',array('required' => false));
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Client',
            'validation_groups' =>function (FormInterface $form) {
                $data = $form->getData();

                if (!$data->getCardNumber()) {
                    return array('registration');
                }
                return array('Default');
            },
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'client';
    }
}

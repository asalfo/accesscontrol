<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Form\ClientTransformer;

class AccessType extends AbstractType
{

    private $manager;

    /**
     * @param ObjectManager $manager
     */
    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locker')
            ->add('memberCard','text',array('mapped'=>false,'required'=>true))
            ->add('mode','hidden',array('mapped'=>false,'required'=>false))
            ->add('client','hidden')
            ->get('client')
            ->addModelTransformer(new ClientTransformer($this->manager));
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function sconfigureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Access'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'access';
    }
}

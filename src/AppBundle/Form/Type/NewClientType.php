<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Membership;
use Doctrine\ORM\EntityRepository;

class NewClientType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('idCardType','choice', array(
                'label' => 'Tipo documento',
                'choices'  => array(1 => 'DNI','2' =>'Tarjeta de Residencia','3' =>'Pasaporte'),
                'required' => true))
            ->add('idCardNumber','text', array('label' => 'Nº documento'))
            ->add('cardNumber',"text",array('label' => 'Nº Socio','required' => false))
            ->add('name','text',array('label' => 'Nombre'))
            ->add('surname','text',array('label' => 'Apellidos'))
            ->add('email','email',array('required' => false))
            ->add('rate', 'entity', array(
                'label' => 'Suscripción',
                'class' => 'AppBundle:Rate',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('r')
                        ->where('r.status = true');
                },
                'required' => false,
                "mapped" => false,
                'placeholder' => false))
            ->add('agree','checkbox',array("mapped" => false,'label'=>'He leído y acepto los términos y condiciones de uso, así como la política de privacidad conforme
a mis preferencias de publicidad que figuran <a href="#" data-toggle="modal" data-target="#myModal">Aqui</a>'));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Client'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'client';
    }
}

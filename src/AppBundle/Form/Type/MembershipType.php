<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Membership;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Form\ClientTransformer;
use Doctrine\ORM\EntityRepository;


class MembershipType extends AbstractType
{

    private $manager;

    /**
     * @param ObjectManager $manager
     */
    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('rate', 'entity', array(
                'label' => 'Tarifa',
                'class' => 'AppBundle:Rate',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('r')
                           ->where('r.status = true');
                },
                'required' => false,
                'placeholder' => false
            ))
            ->add('startDate','datetime', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy HH:mm'
            ))
            ->add('endDate','datetime', array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy HH:mm',
                'read_only' => true
            ))
            ->add('client','hidden')
            ->get('client')
            ->addModelTransformer(new ClientTransformer($this->manager));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Membership'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'membership_form';
    }
}

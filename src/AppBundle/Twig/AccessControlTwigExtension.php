<?php

namespace AppBundle\Twig;

use AppBundle\Entity\Client;

class AccessControlTwigExtension extends  \Twig_Extension
{
    public function getName()
    {
        return 'access_control_twig_extension';
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('idType', array($this, 'idType')),
        );
    }


    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('computeTime',array($this,'computeTime')),
        );
    }

    /**
     * @param $type
     * @return null|string
     */
    public function idType($type)
    {
        switch($type){
            case Client::DNI:
                $result = 'DNI';
                break;
            case Client::NIE:
                $result = 'NIE';
                break;
            case Client::PASSPORT:
                $result = 'PASAPORTE';
                break;
            default:
                $result = null;
        }

        return $result;
    }



    /**
     * @param \DateInterval $timeInterval
     * @return string
     */
    public function computeTime(\DateInterval $timeInterval)
    {
        $time = "";
        $horas = ($timeInterval->d * 24) +$timeInterval->h;
        if($horas > 1){
            $time = $timeInterval->h." Horas ";
        }elseif($horas == 1){
            $time = $timeInterval->h." Hora ";
        }
        if($timeInterval->i > 1){
            $time .= $timeInterval->i." Minutos ";
        }elseif($timeInterval->i == 1){
            $time = $timeInterval->h." Minuto ";
        }

        if($timeInterval->s > 1){
            $time .= $timeInterval->s." Segundos";
        }elseif($timeInterval->s == 1){
            $time = $timeInterval->h." Segundo";
        }

        return $time;
    }


}
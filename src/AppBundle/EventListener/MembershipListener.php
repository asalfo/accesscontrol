<?php

namespace AppBundle\EventListener;

use AppBundle\Event\MembershipEvent;
use AppBundle\Entity\Membership;

class MembershipListener
{

    protected $entityManager;

    /**
     * MembershipListener constructor.
     * @param $entityManager
     */
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }


    public function onPostMembershipCreate(MembershipEvent $event)
    {
        $membership = $event->getMembership();

        if ($membership instanceof Membership) {
            $previousActiveMembership =$this->entityManager->getRepository('AppBundle:Membership')->getPreviousActiveMembership($membership);
            if($previousActiveMembership and ($previousActiveMembership->getId() <> $membership->getId()) ){
                $previousActiveMembership->setEndDate($membership->getStartDate());
                $this->entityManager->persist($previousActiveMembership);
                $this->entityManager->flush();
            }
        }
    }

}
<?php

namespace AppBundle\Behat\Bootstrap;

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

use Behat\Behat\Context\ClosuredContextInterface;
use Behat\Behat\Context\TranslatedContextInterface;
use Behat\Behat\Context\BehatContext;
use Behat\Behat\Hook\BeforeScenario;
use Behat\Behat\Hook\BeforeSuite;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use PHPUnit_Framework_Assert as PHPUnit;
use Symfony\Component\HttpKernel\KernelInterface;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;


/**
 * Defines application features from the specific context.
 */
class WebContext extends MinkContext implements SnippetAcceptingContext,KernelAwareContext
{

    /**
     * @var \Symfony\Component\HttpKernel\KernelInterface $kernel
     */
    private $kernel = null;
    private $users = array();
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @param \Symfony\Component\HttpKernel\KernelInterface $kernel
     *
     * @return null
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @return KernelInterface
     */
    public function getKernel()
    {
        return $this->kernel;
    }


    /**
     * @Given the following system users:
     */
    public function theFollowingSystemUsers(TableNode $table)
    {
        $manipulator = $this->getKernel()->getContainer()->get('fos_user.util.user_manipulator');
        foreach ($table->getHash() as $row) {
            $manipulator->create($row['username'], $row['password'], $row['email'], $row['active'], $row['superadmin']);
            $this->users[$row['username']] = $row;
        }
    }


    /**
     * @Given I am authenticated as :username
     */
    public function iAmAuthenticatedAs($username)
    {
        if (!isset($this->users[$username]['password'])) {
            throw new \OutOfBoundsException('Invalid user ' . $username);
        }

        $this->visit('/login');
        $this->fillField('username', $username);
        $this->fillField('password', $this->users[$username]['password']);
        $this->pressButton('Log in');
    }

    /**
     * @Given I am a receptionist
     */
    public function iAmAReceptionist()
    {
        $manipulator = $this->getKernel()->getContainer()->get('fos_user.util.user_manipulator');
         $manipulator->create("recepcionista", "recep001", "email@recpcion.com", true, false);
    }

    /**
     * @Given I am a administrator
     */
    public function iAmAAdministrator()
    {
        $manipulator = $this->getKernel()->getContainer()->get('fos_user.util.user_manipulator');
         $manipulator->create("admin", "admin001", "email@sys.com", true, true);
    }

    /**
     * @return Behat\Mink\Element\DocumentElement
     */
    protected function getPage()
    {
        return $this->getSession()->getPage();
    }

    /**
     * @Given the following clients exist:
     */
    public function theFollowingClientsExist(TableNode $table)
    {
        $em = $this->getKernel()->getContainer()->get('doctrine')->getManager();
        $clientRepository = $em->getRepository('AppBundle\Entity\Client');
        foreach ($table->getHash() as $row) {
            $clientRepository->create($row);
        }
    }

    /**
     * @Then I should see :arg1 rows in the table
     */
    public function iShouldSeeRowsInTheTable($rows)
    {
        $table = $this->getPage()->find('css', 'table');
        PHPUnit::assertNotNull($table, 'Cannot find a table!');
        PHPUnit::assertCount(intval($rows), $table->findAll('css', 'tbody tr'));
    }

    /**
     * @Then I should see page title :title
     */
    public function iShouldSeePageTitle($title)
    {
        $text = $this->getPage()->find('css', "h1")->getText();
        PHPUnit::assertEquals($text, $title);
    }

    /**
     * @When I wait for :time seconds
     */
    public function iWaitForSeconds($time)
    {
        $this->getSession()->wait($time);
    }


    /**
     * @Then the field :fiels should be :value
     */
    public function theFieldShouldBe($field, $value)
    {
        $field = $this->fixStepArgument($field);
        $value = $this->fixStepArgument($value);
        $this->assertSession()->fieldValueEquals($field, $value);
    }


    /**
     * @BeforeScenario @database,@orm
     */
    public function cleanDatabase()
    {
        /** @var $doctrine \Doctrine\Common\Persistence\ManagerRegistry */
        $doctrine = $this->getKernel()->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $purger = new ORMPurger($em);
        $purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);
        $purger->purge();
    }
}

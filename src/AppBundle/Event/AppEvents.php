<?php


namespace AppBundle\Event;


final class AppEvents
{
    /**
     * The  post_membership_create event is thrown each time an membership is created
     * in the system.
     *
     * The event listener receives an
     * AppBundle\Event\MembershipsEvent instance.
     *
     * @var string
     */
    const POST_MEMBERSHIP_CREATE = 'post_membership_create';

    /**
     * The  post_membership_update event is thrown each time an membership is updated
     * in the system.
     *
     * The event listener receives an
     * AppBundle\Event\MembershipsEvent instance.
     *
     * @var string
     */
    const POST_MEMBERSHIP_UPDATE = 'post_membership_update';
}
<?php


namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity\Membership;

class MembershipEvent extends  Event
{
    protected $membership;

    public function __construct(Membership $membership)
    {
        $this->membership = $membership;
    }

    public function setMembership($membership)
    {
        $this->membership = $membership;
    }


    public function getMembership()
    {
        return $this->membership;
    }
}
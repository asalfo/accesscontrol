<?php
/**
 * Created by PhpStorm.
 * User: salif.guigma
 * Date: 10/5/15
 * Time: 1:49 PM
 */

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
/**
 * @Annotation
 */
class AccessClientConstraint extends  Constraint
{
    public $message = 'El socio ( "%string%") ya esta en local.';

    public function validatedBy()
    {
        return 'access_client_constraint.validator';
    }
}
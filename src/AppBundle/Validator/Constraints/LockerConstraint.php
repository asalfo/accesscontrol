<?php
/**
 * Created by PhpStorm.
 * User: salif.guigma
 * Date: 10/5/15
 * Time: 12:17 PM
 */

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
/**
 * @Annotation
 */
class LockerConstraint extends  Constraint
{
    public $message = 'La taquilla "%string%" esta en uso.';

    public function validatedBy()
    {
        return 'locker_constraint.validator';
    }
}
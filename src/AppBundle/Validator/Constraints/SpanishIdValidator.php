<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;

class SpanishIdValidator extends  ConstraintValidator
{
    public function validate($object, Constraint $constraint)
    {
        $type = $object->getIdCardType();
        $idCardNumber =  $object->getIdCardNumber();
       if(1 == $type) {
           if (!$this->isValidDni($idCardNumber)) {
               $this->context->buildViolation($constraint->message)
                   ->atPath('idCardNumber')
                   ->setParameter('%string%', $idCardNumber)
                   ->addViolation();

           }
       }

        if(2 == $type){
            if (!$this->isValidNie($idCardNumber)) {
                $this->context->buildViolation($constraint->message)
                    ->atPath('idCardNumber')
                    ->setParameter('%string%', $idCardNumber)
                    ->addViolation();

            }
        }
    }


    private function isValidDni($value) {

        $value = strtoupper($value);

        for ($i = 0; $i < 9; $i ++){
            $num[$i] = substr($value, $i, 1);
        }
        $rexp = "/((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)/";
        if (!preg_match($rexp, $value,$matches)){
            return false;
        }
        $rexp = "/(^[0-9]{8}[A-Z]{1}$)/";
        if (preg_match($rexp, $value)){
            if ($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE',substr($value, 0, 8) % 23, 1)){
               return true;
            }else {
                return false;
            }
        }

        $suma = $num[2] + $num[4] + $num[6];
        for ($i = 1; $i < 8; $i += 2){
            $suma += substr((2 * $num[$i]),0,1) + substr((2 * $num[$i]),1,1);
        }
        $n = 10 - substr($suma, strlen($suma) - 1, 1);
        $rexp = "/^[KLM]{1}/";
        if (preg_match($rexp, $value)){
            if ($num[8] == chr(64 + $n)){
               return true;
            }else{
                return false;
            }
        }
        return false;
    }

    private  function  isValidNie($value){

        $value = strtoupper($value);

        for ($i = 0; $i < 9; $i ++){
            $num[$i] = substr($value, $i, 1);
        }
        $rexp = "/((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)/";
        if (!preg_match($rexp, $value,$matches)){
            return false;
        }

        $rexp ="/^[ABCDEFGHJNPQRSUVW]{1}/";
        if (preg_match($rexp, $value)){
            if ($num[8] == chr(64 + $n) || $num[8] ==
                substr($n, strlen($n) - 1, 1)){
                return true;
            }else{
                return false;
            }
        }

        $rexp ="/^[T]{1}/";
        if (preg_match($rexp, $value)){
            if ($num[8] == preg_match('/^[T]{1}[A-Z0-9]{8}$/', $value)){
                return true;
            }else{
                return false;
            }
        }
        $rexp = "/^[XYZ]{1}/";
        if (preg_match($rexp, $value)){
            if ($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE',
                    substr(str_replace(array('X','Y','Z'),
                        array('0','1','2'), $value), 0, 8) % 23, 1)){
                return true;
            }else{
                return false;
            }
        }
    }
}

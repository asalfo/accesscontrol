<?php
/**
 * Created by PhpStorm.
 * User: salif.guigma
 * Date: 10/5/15
 * Time: 1:55 PM
 */

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;

class AccessClientConstraintValidator extends  ConstraintValidator
{
    private $objectManager;

    /**
     * AccessClientConstraintValidator constructor.
     * @param $objectManager
     */
    public function __construct($objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function validate($value, Constraint $constraint)
    {
        $client = $this->objectManager->getRepository('AppBundle:Access')->findOneByClientId($value);

        if(null != $client){
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', $value)
                ->addViolation();
        }
    }


}
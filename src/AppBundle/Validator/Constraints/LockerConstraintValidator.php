<?php
/**
 * Created by PhpStorm.
 * User: salif.guigma
 * Date: 10/5/15
 * Time: 12:19 PM
 */

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Doctrine\Common\Persistence\ObjectManager;

class LockerConstraintValidator extends  ConstraintValidator
{

    private  $objectManager;

    /**
     * LockerConstraintValidator constructor.
     * @param $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function validate($value, Constraint $constraint) {

        $locker = $this->objectManager->getRepository('AppBundle:Access')->findOneByLockerNumber($value);

        if(null != $locker){
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', $value)
                ->addViolation();
        }

    }
}
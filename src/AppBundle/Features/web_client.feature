Feature: Clients management
  In order to allow clients operate properly in the club
  As a systeme user
  I need to be able to add, edit and delete client

  Background:
    Given the following system users:
    | username  | password  | email            | active  | superadmin |
    | admin     | admin     | admin@system.com | 1       | 1          |
    | reception | recep     | recep@system.com | 1       | 0          |

  @orm
  Scenario: Checking admin user dashboard
    Given I am authenticated as "admin"
    Then I should see "Dashboard"
    And I should see "Socios"
    And I should see "Movimientos"
    And I should see "Passes"


  @orm
  Scenario: Checking reception user dashboard
    Given I am authenticated as "reception"
    Then I should see "Dashboard"
    And I should see "Socios"
    And I should see "Movimientos"
    And I should not see "Passes"

  @orm
  Scenario: Adding a new user
    Given I am authenticated as "reception"
    Then I follow "Socios"
    When I wait for 1 seconds
    And  I follow "Nuevo"
    And  I select "2" from "client_idCardType"
    And  I fill in "client_idCardNumber" with "X5704206E"
    And  I fill in "client_cardNumber" with "5789541111"
    And  I fill in "client_name" with "Doudou"
    And  I fill in "client_surname" with "Del pinozo real"
    And  I fill in "client_email" with "doudou@gmail.com"
    And  I check "client_agree"
    And  I press "client_submit"
    When I wait for 6 seconds
    And I should see "Socio creado correctamente!"
    And the field "Nombre" should be "Doudou"
    And the field "Apellidos" should be "Del pinozo real"
    And the field "Email" should be "doudou@gmail.com"



  @orm
  Scenario: Seeing a list of existing clients
    Given I am authenticated as "reception"
    And the following clients exist:
    | idCardType | idCardNumber | name      | surname  | email            | state |
    | 2          | Z4853287Q    | Pepe      | Puntazo  | pepe@test.com    | 1     |
    | 1          | 29521741E    | fulano    | Robert   | robert@testr.com | 0     |
    | 3          | Z4853287     | manolo    | Marciano | pepe@test.com    | 1     |
    And I am on "/client"
    Then I should see 2 rows in the table
    And I should see "Pepe"
    And I should see "pepe@test.com"
    And I should not see "29521741E"
    And I should see "Z4853287"
    And I should see "Pasaporte"
    And I should not see "DNI"
    And I should see "NIE"
    But I should not see "Marcianito"

  @orm
  Scenario: checking pending users
    Given I am authenticated as "reception"
    And the following clients exist:
      | idCardType | idCardNumber | name      | surname  | email            | state |
      | 2          | Z4853287Q    | Pepe      | Puntazo  | pepe@test.com    | 0     |
      | 1          | 29521741E    | fulano    | Robert   | robert@testr.com | 0     |
      | 3          | Z4853287     | manolo    | Marciano | pepe@test.com    | 0     |
    And   I am on "client/pending"
    Then  I should see 3 rows in the table
    And  I follow "Modificar"
    And  I fill in "client_cardNumber" with "00007844211"
    And  I press "client_submit"
    And  I am on "client/pending"
    Then I should see 2 rows in the table
    When I am on "/client"
    Then I should see 1 rows in the table

  @orm
  Scenario: Registing a new user
    Given I am on "client/register"
    And  I select "2" from "client_idCardType"
    And  I fill in "client_idCardNumber" with "X5704206E"
    And  I fill in "client_name" with "Doudou"
    And  I fill in "client_surname" with "Del pinozo real"
    And  I fill in "client_email" with "doudou@gmail.com"
    And  I check "client_agree"
    And  I press "client_submit"
    And  I should see "Operación procesada correctamente! "

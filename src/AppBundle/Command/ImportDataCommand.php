<?php

namespace AppBundle\Command;
use AppBundle\Entity\Rate;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Ddeboer\DataImport\Reader\CsvReader;
use AppBundle\Entity\Client;
use AppBundle\Entity\Membership;

class ImportDataCommand extends ContainerAwareCommand
{
    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('app:data:import')
            ->setDescription('Import data form csv')
            ->setDefinition(array(
                new InputArgument('file_path', InputArgument::REQUIRED, 'The path of data file (.csv)'),
            ))
            ->setHelp(<<<EOT
The <info>app:data:import</info> command importes data form .csv files to the database:

  <info>php app/console app:data:import path-to-csv-file</info>
EOT
            );
    }


    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filePath = $input->getArgument('file_path');

        $file = new \SplFileObject($filePath);
        $reader = new CsvReader($file);

        $count = 1;
        $duplication=0;

        foreach ($reader as $row) {
            $this->addClient($row,$output,$duplication);
            $count++;
        }
        $output->writeln(sprintf(' "%s" have been added from File "%s" has been imported. with "%s" duplications detected ', $count,$filePath,$duplication));
    }


    /**
     * @param $data
     * @param $duplication
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     */
    private  function addClient($data,$output,&$duplication){

        $manager = $this->getContainer()->get('doctrine')->getManager();
        $rate =  $manager->getRepository('AppBundle:Rate')->find(rand(1,7));
        if(!$manager->isOpen())
        {
            $this->getContainer()->get('doctrine')->resetManager();
            $manager = $this->getContainer()->get('doctrine')->getManager();
        }

        $client = new Client();
        if($data[4]) {
            $output->writeln($data[4]);
            $idType = (in_array(strtoupper($data[4]{0}), array('X', 'Y', 'Z')) ? 2 : 1);
            $client->setIdCardType($idType);
            $client->setIdCardNumber($data[4]);
        }
        $client->setCardNumber($data[0]);
        $client->setName($data[1]);
        $client->setSurname($data[2]." ".$data[3]);
        $client->setEmail($data[7]);
        $date = str_replace('/', '-', $data[5]);
        $client->setCreatedAt(new \DateTime(date('Y-m-d', strtotime($date))));
        $client->setState(Client::STATE_ACTIVE);
        $manager->persist($client);


        $membership = new Membership();
        $membership->setClient($client);
        //$membership->setRate($rate);

        $date = str_replace('/', '-', $data[5]);
        $membership->setStartDate(new \DateTime(date('Y-m-d', strtotime($date))));
        $date = str_replace('/', '-', $data[6]);
        $membership->setEndDate(new \DateTime(date('Y-m-d', strtotime($date))));
        $membership->setStatus(false);

        $manager->persist($membership);
        try {
        $manager->flush();
        } catch(\Doctrine\DBAL\DBALException $e)
        {
            if($e->getPrevious()->getCode() != '23000')
            {
                throw $e;
            }
            else
            {
                $duplication++;
                $output->writeln($e->getMessage());
            }
        }
    }
}
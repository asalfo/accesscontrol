<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Rate;
use AppBundle\Form\Type\RateType;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Rate controller.
 *
 * @Route("/rate")
 */
class RateController extends Controller
{

    /**
     * Lists all Rate entities.
     *
     * @Route("/", name="rate")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Rate')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Rate entity.
     *
     * @Route("/", name="rate_create")
     * @Method("POST")
     * @Template("AppBundle:Rate:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Rate();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->addFlash('notice', 'Tarifa añadia correctamente');
            return $this->redirect($this->generateUrl('rate_edit', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Rate entity.
     *
     * @param Rate $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Rate $entity)
    {
        $form = $this->createForm(new RateType(), $entity, array(
            'action' => $this->generateUrl('rate_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Añadir'));

        return $form;
    }

    /**
     * Displays a form to create a new Rate entity.
     *
     * @Route("/new", name="rate_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Rate();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }



    /**
     * Displays a form to edit an existing Rate entity.
     *
     * @Route("/{id}/edit", name="rate_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Rate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Rate entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Rate entity.
    *
    * @param Rate $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Rate $entity)
    {
        $form = $this->createForm(new RateType(), $entity, array(
            'action' => $this->generateUrl('rate_update', array('id' => $entity->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Rate entity.
     *
     * @Route("/{id}", name="rate_update")
     * @Method("POST")
     * @Template("AppBundle:Rate:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Rate')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Rate entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);


        if ($editForm->isValid()) {
            $em->flush();
            $this->addFlash('notice', 'Datos modificados correctamente');
            return $this->redirect($this->generateUrl('rate_edit', array('id' => $id)));
        }else{

        }

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Rate entity.
     *
     * @Route("/{id}", name="rate_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Rate')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Rate entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('rate'));
    }

    /**
     * Creates a form to delete a Rate entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('rate_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Displays a form to edit an existing Rate entity.
     *
     * @Route("/{id}", name="rate_retrieve",requirements={
     *     "id": "\d+"
     * }))
     * @Method("GET")
     */
    public function rateAction($id){

        $em = $this->getDoctrine()->getManager();

        $rate = $em->getRepository('AppBundle:Rate')->findOneBy(array('id'=>$id,'status'=>true));

        if (!$rate) {
            $jsonContent = json_encode(array('msg'=>printf("Membership with id %s not found ",$id)));
            $response = new Response($jsonContent,404,array('Content-Type'=>'application/json'));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }

        return  new JsonResponse($rate->toArray());
    }

}

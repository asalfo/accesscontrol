<?php

namespace AppBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Client;
use AppBundle\Form\Type\ClientType;
use AppBundle\Form\Type\NewClientType;
use AppBundle\Entity\Membership;

/**
 * Client controller.
 *
 * @Route("/client")
 */
class ClientController extends BaseController
{



    /**
     * Lists all Clients data.
     *
     * @Route("/json", name="client_json")
     * @Method("POST")
     * @Template()
     */
    public function listAction(Request $request){
        $clientManager = $this->get('client.manager');
        $params = $this->getDataTableParametersFromRequest($request);
        $whereAll = 'state = '.Client::STATE_ACTIVE;
        $data = $clientManager->complexData($params,$clientManager->getColumns(),null,$whereAll);
        return new JsonResponse($data);

    }

    /**
     * Lists all Client entities.
     *
     * @Route("/", name="client")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Client')->findBy(array('state'=>Client::STATE_ACTIVE),null, 50,0);

        return array(
            'entities' => $entities,
        );
    }


    /**
     * Lists all Client entities.
     *
     * @Route("/pending", name="client_pending")
     * @Method("GET")
     * @Template()
     */
    public function pendingAction()
    {
        $em = $this->getDoctrine()->getManager();
        $pending = $em->getRepository('AppBundle:Client')->findBy(array('cardNumber'=>null,'state'=>Client::STATE_PENDING),null, 50,0);
        return array(
            'entities' => $pending,
        );
    }


    /**
     * Creates a new Client entity.
     *
     * @Route("/register", name="client_register")
     * @Template("AppBundle:Client:register.html.twig")
     */
    public function registerAction(Request $request)
    {
        $client = new Client(Client::STATE_PENDING);
        $form = $this->createCreateForm($client);
        $form->remove('cardNumber');

        if($request->getMethod() =='POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($client);
                $em->flush();
                $this->createAddMembership($client, $request);
                $this->addFlash('notice', 'Operación procesada correctamente!');
                return $this->render('AppBundle:Client:success.html.twig',array('client'=>$client));
            }
        }
        return array(
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a new Client entity.
     *
     * @Route("/new", name="client_new")
     * @Template("AppBundle:Client:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $client = new Client(Client::STATE_ACTIVE);
        $form = $this->createCreateForm($client);
        if($request->getMethod() =='POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($client);
                $em->flush();
                $this->createAddMembership($client, $request);
                $this->addFlash('notice', 'Socio creado correctamente!');
                return $this->redirect($this->generateUrl('client_edit', array('id' => $client->getId())));
            }
        }

        return array(
            'entity' => $client,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Client entity.
     *
     * @param Client $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Client $entity)
    {
        $form = $this->createForm(new NewClientType(), $entity, array(
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array('label' => 'Guardar',"attr"=>array("class"=>"btn btn-success")));
        return $form;
    }



    /**
     * Displays a form to edit an existing Client entity.
     *
     * @Route("/{id}/edit", name="client_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $client = $em->getRepository('AppBundle:Client')->find($id);

        if (!$client) {
            throw $this->createNotFoundException('Unable to find Client entity.');
        }

        $memberships = $em->getRepository('AppBundle:Membership')->findByClient($client);
        $editForm = $this->createEditForm($client);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'client'      => $client,
            'memberships' => $memberships,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Client entity.
    *
    * @param Client $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Client $entity)
    {
        $form = $this->createForm(new ClientType(), $entity, array(
            'action' => $this->generateUrl('client_update', array('id' => $entity->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar',"attr"=>array("class"=>"btn btn-success")));

        return $form;
    }
    /**
     * Edits an existing Client entity.
     *
     * @Route("/{id}", name="client_update",requirements={
     *     "id": "\d+"
     * })
     * @Method("POST")
     * @Template("AppBundle:Client:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $client = $em->getRepository('AppBundle:Client')->find($id);

        if (!$client) {
            throw $this->createNotFoundException('Unable to find Client entity.');
        }
        
        $editForm = $this->createEditForm($client);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->addFlash('notice', 'Datos modificados correctamente');
            return $this->redirect($this->generateUrl('client_edit', array('id' => $id)));
        }
        $memberships = $em->getRepository('AppBundle:Membership')->findByClient($client);
        return array(
            'client'      => $client,
            'memberships' => $memberships,
            'form'   => $editForm->createView(),
        );
    }

    /**
     * Edits an existing Client entity.
     *
     * @Route("/updateData/{id}", name="client_update_data",requirements={
     *     "id": "\d+"
     * })
     * @Method("POST")
     *
     */
    public function updateDataAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $validator = $this->get('validator');

        $errors = null;
        $msg = null;
        $client = $em->getRepository('AppBundle:Client')->find($id);
        $clientdata = null;
        if (!$client) {
            throw $this->createNotFoundException('Unable to find Client entity.');
        }
        $data = $request->request->get('client');

        $client->setIdCardType($data['idCardType']);
        $client->setIdCardNumber($data['idCardNumber']);
        $client->setCardNumber($data['cardNumber']);
        $client->setName($data['name']);
        $client->setSurname($data['surname']);
        if(strlen($data['email'])){
            $client->setEmail($data['email']);
        }

        $violations = $validator->validate($client);
        if (count($violations) == 0) {
            $em->merge($client);
            $em->flush();
            $msg = "Datos modificados correctamente";

            $membership = $em->getRepository('AppBundle:Membership')->getLastActiveMembership($client);
            $mdata = array(
                'type' => Membership::$MEMBERSHIP_TYPES[$membership->getType()],
                'endDate' => $membership->getEndDate()->format("d-m-Y H:i")
            );
            $clientdata = array(
                'id' => $client->getId(),
                'idCardNumber' => $client->getIdCardNumber(),
                'name' => $client->getName(),
                'surname' => $client->getSurname(),
                'cardNumber' => $client->getCardNumber(),
                'membership' => $mdata
            );
        }else{
            foreach($violations as $e){
                $errors[$e->getPropertyPath()] = $e->getMessage();
            }
        }

        return new JsonResponse(array('errors'=>$errors,'msg'=>$msg,'client'=>$clientdata));

    }


    /**
     * Deletes a Client entity.
     *
     * @Route("/{id}", name="client_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Client')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Client entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('client'));
    }



    /**
     * @Route("/form/{id}", name="client_form")
     * @Method("GET")
     * @Template("AppBundle:Client:data.html.twig")
     * @param $id
     * @return mixed
     */
    public function clientFormAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $client = $em->getRepository('AppBundle:Client')->find($id);

        if (!$client) {
            throw $this->createNotFoundException('Unable to find Client entity.');
        }
        $editForm = $this->createEditForm($client);

        return
            array(
                'client' => $client,
                'form' => $editForm->createView()
            );

    }

    /**
     * Creates a form to delete a Client entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('client_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }


   private function createAddMembership(Client $client, Request $request)
   {
       $data = $request->request->get('client');

        if(!empty($data['rate'])) {
            $em = $this->getDoctrine()->getManager();
            $rate = $em->getRepository('AppBundle:Rate')->find($data['rate']);
            $membership = new Membership($rate, $client);
            $em->persist($membership);
            $em->flush();
        }

   }
}

<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Client;

class DashboardController extends Controller
{
    /**
     * @Route("/", name="dashboard")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $accesses = $em->getRepository('AppBundle:Access')->findTodayAccesses();
        $anonyAccesses = $em->getRepository('AppBundle:SpecialAccess')->findTodayAccesses();
        $em = $this->getDoctrine()->getManager();
        $pending = $em->getRepository('AppBundle:Client')->findBy(array('cardNumber'=>null,'state'=>Client::STATE_PENDING),null, 50,0);

        return array(
            'accesses' => $accesses,
            'anonyAccesses' => $anonyAccesses,
            'pending' => $pending
        );

    }



}

<?php

namespace AppBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;



class BaseController extends  Controller
{

    public function getDataTableParametersFromRequest(Request $request)
    {

        $params = array();
        $params['start'] = $request->request->get('start');
        $params['length'] = $request->request->get('length');
        $params['order'] = $request->request->get('order');
        $params['columns'] = $request->request->get('columns');
        $params['search'] = $request->request->get('search');
        $params['draw'] = $request->request->get('draw');
        $params['rangeFilters'] = $request->request->get('rangeFilters');

        return $params;

    }

    protected function processFormErrors($form){
        $errors = array();
        foreach ($form->getErrors(true,false) as $error) {
            if ($error instanceof FormError) {
                if(null != $error->getCause()){
                    $field = $error->getCause()->getInvalidValue();
                    if(null != $field && is_array($field)){
                        $errors[key($field)] = $error->getMessage();
                    }else{
                        $errors[] = $error->getMessage();
                    }
                }else{
                    $errors[] = $error->getMessage();
                }

            } else{

                $errors[] = (string) $error;
            }
        }
        return $errors;
    }
}
<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Access;
use AppBundle\Form\Type\AccessType;

/**
 * Access controller.
 *
 * @Route("/access")
 */
class AccessController extends BaseController
{


    /**
     * List all accesses
     * @Route("/json", name="access_json")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
    public function listAction(Request $request){
        $clientManager = $this->get('access.manager');
        $params = $this->getDataTableParametersFromRequest($request);
        $data = $clientManager->complexData($params,$clientManager->getColumns());
        return new JsonResponse($data);

    }

    /**
     * Lists all Access entities.
     *
     * @Route("/", name="access")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $accesses = $em->getRepository('AppBundle:Access')->findTodayAccesses();

        return array(
            'entities' => $accesses,
        );
    }

    /**
     * Displays a form to create a new Access entity.
     *
     * @Route("/new", name="access_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Access();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'has_errors' => (count($form->getErrors()) ? true: false)
        );
    }
    /**
     * Creates a new Access entity.
     *
     * @Route("/create", name="access_create")
     * @Method("POST")
     * @Template("AppBundle:Access:new.html.twig")
     */
    public function createAction(Request $request)
    {

        $access = new Access();
        $form = $this->createCreateForm($access);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->addFlash('notice', 'Taquilla asignada correctamente!');
            $em = $this->getDoctrine()->getManager();
            $em->persist($access);
            $em->flush();
            return $this->redirect($this->generateUrl('access_new'));

        }

        return array(
            'entity' => $access,
            'form'   => $form->createView(),
            'has_errors' => true
        );


    }


    /**
     * Ends access
     *
     * @Route("/exit", name="access_exit")
     * @Method("GET")
     * @Template("AppBundle:Access:exit.html.twig")
     */
    public function accessEndAction(Request $request)
    {

    }

    /**
     * Creates a new Access entity.
     *
     * @Route("/end", name="access_end")
     * @Method("POST")
     * @Template("AppBundle:Access:exit.html.twig")
     */
    public function exitAction(Request $request)
    {
        $data = $request->request->get('access');
        $em = $this->getDoctrine()->getManager();
        $access = $em->getRepository('AppBundle:Access')->findOneByLockerNumber($data['locker']);
        if(!$access){
            $access = $em->getRepository('AppBundle:SpecialAccess')->findOneByLockerNumber($data['locker']);
        }
        if(null !== $access) {
            $access->setEndDate(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($access);
            $em->flush();
            $this->addFlash('notice', 'La Taquilla '.$data['locker'].' ha sido liberada.' );
        }else{
            $this->addFlash('warning', 'La Taquilla '.$data['locker'].' no esta ocupada.' );
        }

        return $this->redirect($this->generateUrl('access_exit'));
    }


    /**
     * Creates a form to create a Access entity.
     *
     * @param Access $access The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Access $access)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new AccessType($em), $access, array(
            'action' => $this->generateUrl('access_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }


    /**
     * Displays a form to edit an existing Access entity.
     *
     * @Route("/{id}/edit", name="access_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Access')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Access entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Access entity.
    *
    * @param Access $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Access $entity)
    {
        $form = $this->createForm(new AccessType(), $entity, array(
            'action' => $this->generateUrl('access_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Access entity.
     *
     * @Route("/{id}", name="access_update")
     * @Method("PUT")
     * @Template("AppBundle:Access:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Access')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Access entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('access_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Access entity.
     *
     * @Route("/{id}", name="access_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Access')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Access entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('access'));
    }

    /**
     * Creates a form to delete a Access entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('access_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * @Route("/assign/{id}", name="assign_locker")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
    public function asignLockerAction(Request $request, $id)
    {
        $validator = $this->get('validator');
        $em = $this->getDoctrine()->getManager();
        $error = null;
        $msg = null;
        $errors =null;
        $data = $request->request->get('access');
        $locker = intval($data['locker']);
        if (!$locker) {
            $error = 'Numero de taquilla invalido.';
        } else {
            $client = $em->getRepository('AppBundle:Client')->find($id);
            if ($client) {
                $access = $em->getRepository('AppBundle:Access')->findOneByClientId($client);
                if (null != $access) {
                    $error = 'El client ya esta en el local';
                } else {

                    $access = new Access();
                    $access->setClient($client);
                    $access->setLocker($data['locker']);
                    $errors = $validator->validate($access);
                    if (count($errors) == 0) {
                        $em->persist($access);
                        $em->flush();
                        $msg = 'Taquilla ' . $data['locker'] . ' asignada.';
                    } else {

                        $error = 'La taquilla ' . $data['locker'] . ' esta ocupada.';
                    }
                }
            } else {
                $error = 'no se ha encontrado ningún socio.';
            }
        }

        return new JsonResponse(array('error' => $error, 'msg' => $msg,'bb'=>$errors));
    }
}

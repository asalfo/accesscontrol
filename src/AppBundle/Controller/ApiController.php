<?php
/**
 * Created by PhpStorm.
 * User: asalfo
 * Date: 09/12/15
 * Time: 18:35
 */

namespace AppBundle\Controller;

use AppBundle\Entity\SpecialAccess;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Access;
use Symfony\Component\HttpFoundation\Response;
use JMS\Serializer\SerializationContext;
use AppBundle\Form\Type\ClientType;
use AppBundle\Form\Type\NewClientType;
use AppBundle\Entity\Client;



/**
 * Class ApiController
 * @package AppBundle\Controller
 *
 * @Route("/api/v1")
 */
class ApiController extends  BaseController
{

    /**
     * @Route("/locker/assign")
     * @Method("POST")
     * @return JsonResponse
     */
    public function assignLockerAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->get("data");

        $cardNumber = $data["cardNumber"];
        $locker = $data["locker"];
        $mode = $data["mode"];
        $special_access = $em->getRepository('AppBundle:SpecialAccess')->findCurrentLocker($locker);
        $access = $em->getRepository('AppBundle:Access')->findCurrentLocker($locker);

        if ($access || $special_access){
            $result =array("msg"=> sprintf("La taquilla %d esta ocupada. Por favor elije otra",$locker),"status"=>Response::HTTP_FOUND);
            return new JsonResponse($result);
        }
        
        if($mode) {
            try {
                $access = new SpecialAccess($locker);
                $em->persist($access);
                $em->flush();

            } catch (\Exception $ex) {

                $result = array("msg" => $ex->getMessage(),
                    "status" => Response::HTTP_INTERNAL_SERVER_ERROR);
                return new JsonResponse($result, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }else{
            $client = $em->getRepository('AppBundle:Client')->findClientByCardnumber($cardNumber);
            if (!$client) {
                $result = array("msg" => sprintf("No se ha encontrado ningun socio con tarjeta = %s", $cardNumber), "status" => Response::HTTP_NOT_FOUND);
                return new JsonResponse($result, Response::HTTP_NOT_FOUND);
            }

            $access = $em->getRepository('AppBundle:Access')->findCurrentClient($client->getId());
            if ($access) {
                $result = array("msg" => sprintf("El socio %s esta en el local.", $client), "status" => Response::HTTP_FOUND);
                return new JsonResponse($result);
            }


            try {
                $access = new Access($client, $locker);
                $em->persist($access);
                $em->flush();

            } catch (\Exception $ex) {

                $result = array("msg" => $ex->getMessage(),
                    "status" => Response::HTTP_INTERNAL_SERVER_ERROR);
                return new JsonResponse($result, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
        $result = array("msg" => sprintf("La taquilla %d ha sido asignada correctamente", $locker),
                        "status" => 200);
        return new JsonResponse($result);

    }

    /**
     * Finds a client entity.
     *
     * @Route("/client/membercard/{cardNumber}", name="app_client_by_membercard")
     * @Method("GET")
     */
    public function dataAction($cardNumber,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $serializer = $this->get('jms_serializer');
        $mode = $request->query->get('mode');

        if($mode !== null){
            $client = $this->createAnonymousClient($cardNumber);
        }else{
            
            $client = $em->getRepository('AppBundle:Client')->findClientByCardnumber($cardNumber);
        }

        if (!$client) {
            $reponse =array("msg"=> sprintf("No se ha encontrado ningun socio con tarjeta = %s",$cardNumber), "status"=>Response::HTTP_NOT_FOUND);
            return new JsonResponse($reponse,Response::HTTP_NOT_FOUND);
        }

        $data = array('msg' =>"socio encontrado", 'data' => $client,'membership'=>$client->getActiveMembership(),"status"=>Response::HTTP_OK);
        $reponse = $serializer->serialize($data,'json',SerializationContext::create()->setGroups(array('details')));

        return new Response($reponse,Response::HTTP_OK,array('Content-Type'=>'application/json'));
    }



    /**
     * Finds a client.
     *
     * @Route("/client/{id}", name="api_client")
     * @Method("GET")
     */
    public function clientAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $serializer = $this->get('jms_serializer');

        $client = $em->getRepository('AppBundle:Client')->find($id);

        if (!$client) {
            $reponse =array("msg"=> sprintf("No se ha encontrado ningun socio con id = %d",$id), "status"=>Response::HTTP_NOT_FOUND);
            return new JsonResponse($reponse,Response::HTTP_NOT_FOUND);
        }

        $data = array('msg' =>"socio encontrado", 'data' => $client,'membership'=>$client->getActiveMembership(),"status"=>Response::HTTP_OK);
        $reponse = $serializer->serialize($data,'json',SerializationContext::create()->setGroups(array('details')));

        return new Response($reponse,Response::HTTP_OK,array('Content-Type'=>'application/json'));
    }

    /**
     * Finds a client and update his datas.
     *
     * @Route("/client", name="api_client_create")
     * @Method("POST")
     */
    public  function createClientAction(Request $request){

        $client = new Client();
        $client->setState("1");
        $form = $this->createForm(NewClientType::class,$client,array('csrf_protection' => false));
        $data= json_decode($request->getContent(),true);

        $form->submit($data);

        if ($form->isValid()) {
            $client = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($client);
            $em->flush();
            $reponse =array("msg"=> "Socio creado correctamente", "status"=>Response::HTTP_CREATED);
            $jsonReponse = new JsonResponse($reponse,Response::HTTP_CREATED);
            $jsonReponse->headers->set("Location",$this->generateUrl('api_client', array('id' => $client->getId())));
            return $jsonReponse;
        }else{

            $errors = $this->processFormErrors($form);
            $reponse =array("msg"=> "No se ha podido guardar los datos","errors"=>$errors, "status"=>Response::HTTP_INTERNAL_SERVER_ERROR);
            return new JsonResponse($reponse,Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * Finds a client and update his datas.
     *
     * @Route("/client/{id}", name="api_client_update")
     * @Method("PUT")
     */
    public function updateClientAction(Request $request,$id){
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('AppBundle:Client')->find($id);
        if (!$client) {
            $reponse =array("msg"=> sprintf("No se ha encontrado ningun socio con id = %d",$id), "status"=>Response::HTTP_NOT_FOUND);
            return new JsonResponse($reponse);
        }

        $form = $this->createForm(ClientType::class, $client,array('csrf_protection' => false));
        $data= json_decode($request->getContent(),true);
        unset($data["_token"]);

        $form->submit($data);

        if ($form->isValid()) {
            $em->persist($client);
            $em->flush();
            $reponse =array("msg"=> "Datos modificados correctamente", "status"=>Response::HTTP_CREATED);
            $jsonReponse = new JsonResponse($reponse,Response::HTTP_CREATED);
            $jsonReponse->headers->set("Location",$this->generateUrl('api_client', array('id' => $client->getId())));
            return $jsonReponse;
        }else{

            $errors = $this->processFormErrors($form);

            $reponse =array("msg"=> "No se ha podido guardar los datos","errors"=>$errors, "status"=>Response::HTTP_INTERNAL_SERVER_ERROR);
            return new JsonResponse($reponse);
        }

    }

    private  function createAnonymousClient($cardNumber){

        $client = new Client();
        $client->setCardNumber($cardNumber);
        $client->setIdCardType(Client::PASSPORT);
        $client->setIdCardNumber($cardNumber);
        $client->setName('Anonymous');
        $client->setSurname('Anonymous');
        return $client;
    }

}
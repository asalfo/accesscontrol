<?php
/**
 * Created by PhpStorm.
 * User: asalfo
 * Date: 21/11/15
 * Time: 09:40
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\RateRepository")
 * @ORM\Table(name="rate")
 * @UniqueEntity("title",message="Este titulo esta ya en el sistema. Porfavor use uno distinto.")
 */
class Rate
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"list", "details"})
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(type="string",length=9, unique=true)
     * @Groups({"list", "details"})
     */
    protected $title;


    /**
     * @var integer
     * @Assert\NotBlank()
     * @ORM\Column(type="integer")
     * @Groups({"list", "details"})
     */
    protected $duration;

    /**
     * @ORM\OneToMany(targetEntity="Membership", mappedBy="rate")
     * @Groups({"list"})
     */
    protected $memberships;
    /**
     * @var boolean
     * @ORM\Column(type="boolean",nullable=true)
     * @Groups({"list", "details"})
     */
    protected $status;


    /**
     * @return string
     */

    public function  __toString()
    {

        return strtoupper($this->title);
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Rate
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Rate
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Rate
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param boolean $status
     * @return Rate
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;

    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     * @return Rate
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMembership()
    {
        return $this->memberships;
    }

    /**
     * @param mixed $membership
     * @return Rate
     */
    public function setMembership($membership)
    {
        $this->memberships = $membership;
        return $this;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->memberships = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add membership
     *
     * @param \AppBundle\Entity\Membership $membership
     *
     * @return Rate
     */
    public function addMembership(\AppBundle\Entity\Membership $membership)
    {
        $this->memberships[] = $membership;

        return $this;
    }

    /**
     * Remove membership
     *
     * @param \AppBundle\Entity\Membership $membership
     */
    public function removeMembership(\AppBundle\Entity\Membership $membership)
    {
        $this->memberships->removeElement($membership);
    }


    public function toArray()
    {
        return array('id' => $this->getId(),
            'title' => $this->getTitle(),
            'price' => $this->getPrice(),
            'duration' => $this->getDuration(),
            'status' => $this->getStatus());
    }

    /**
     * Get memberships
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMemberships()
    {
        return $this->memberships;
    }
}

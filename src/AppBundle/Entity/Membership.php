<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\MembershipRepository")
 * @ORM\Table(name="membership")
 * @ORM\HasLifecycleCallbacks()
 */
class Membership
{

    const NORMAL           = 1;
    const VIP_ONE_WEEK     = 2;
    const VIP_ONE_MONTH    = 3;
    const VIP_THREE_MONTHS = 4;
    const VIP_SIX_MONTHS   = 5;
    public static $MEMBERSHIP_TYPES = array(self::NORMAL => 'Normal', self::VIP_ONE_WEEK => 'VIP (1 Semana)',self::VIP_ONE_MONTH=> 'VIP (1 Mes)',
                                   self::VIP_THREE_MONTHS => 'VIP (3 Meses)', self::VIP_SIX_MONTHS => 'VIP (6 meses)');
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"list", "details"})
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Rate", inversedBy="memberships",cascade={"persist"})
     * @ORM\JoinColumn(name="rate_id", referencedColumnName="id")
     * @Groups({"list", "details"})
     */
    protected $rate;
    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="memberships")
     * @Groups({"list"})
     */
    protected $client;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank
     * @Groups({"list", "details"})
     */
    protected $startDate;
    /**
     * @ORM\Column(type="datetime",nullable=true)
     * @Groups({"list", "details"})
     */
    protected $endDate;

    /**
     * @ORM\Column(type="boolean")
     *     @Groups({"list", "details"})
     */
    protected $status;

    /**
     * Membership constructor.
     * @param null $type
     * @param null $cardNumber
     * @param null $client
     */
    public function __construct($rate=null,$client=null)
    {
        $this->rate = $rate;
        $this->client = $client;
        $this->setStartDate(new \Datetime());
        $this->setStatus(true);
    }


    public  function  __toString()
    {
       return $this->getRate();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return Membership
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return Membership
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     * @return Membership
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return mixed
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param mixed $rate
     * @return Membership
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
        return $this;
    }



    public function isActive(){
      return $this->status;

    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Membership
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }


    /**
     * @ORM\PrePersist
     */
    public function calculateEndDate(){
        if($this->getEndDate() == null) {
            $duration = $this->getRate()->getDuration();
            if ($this->getStartDate() !== null and  $duration !== null) {
                $startDate = clone $this->getStartDate();
                $endDate = null;
                $str= "P".$duration."D";
                $endDate = $startDate->add(new \DateInterval($str));

               $this->setEndDate($endDate);

            }
        }
    }
}

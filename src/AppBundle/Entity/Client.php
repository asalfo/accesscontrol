<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AppAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation as Serializer;
use Doctrine\Common\Collections\Criteria;
/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ClientRepository")
 * @ORM\Table(name="client",indexes={@ORM\Index(name="client_idx", columns={"id_card_number","card_number"})})
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("idCardNumber",message="Este Numéro de documento ha sido usado. Por favor use uno distinto.")
 * @UniqueEntity("cardNumber")
 * @AppAssert\SpanishId
 *
 */
class Client
{

    const DNI           = 1;
    const NIE           = 2;
    const PASSPORT      = 3;
    const STATE_PENDING = 0;
    const STATE_ACTIVE  = 1;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"list", "details"})
     */
    protected $id;

    /**
     * @Assert\NotBlank(message="Campo obligatorio")
     * @ORM\Column(type="string",length=1)
     * @Serializer\Groups({"list", "details"})
     * @Serializer\SerializedName("idCardType")
     */
    protected $idCardType;
    /**
     * @Assert\NotBlank(message="Campo obligatorio")
     * @ORM\Column(type="string", length=9, unique=true)
     * @Serializer\Groups({"list", "details"})
     * @Serializer\SerializedName("idCardNumber")
     */
    protected $idCardNumber;

    /**
     * @ORM\Column(type="string", length=15,nullable=true)
     * @Serializer\Groups({"list", "details"})
     * @Serializer\SerializedName("cardNumber")
     */
    protected $cardNumber;
    /**
     * @Assert\NotBlank(message="Campo obligatorio")
     * @ORM\Column(type="string", length=100)
     * @Serializer\Groups({"list", "details"})
     */
    protected $name;
    /**
     * @Assert\NotBlank(message="Campo obligatorio")
     * @ORM\Column(type="string", length=100)
     * @Serializer\Groups({"list", "details"})
     */
    protected $surname;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\Email()
     * @Serializer\Groups({"list", "details"})
     */
    protected $email;

    /**
     * @var string $stat
     * @ORM\Column(type="boolean")
     * @Serializer\Groups({"list", "details"})
     */
    protected $state;

    
    /**
     * @ORM\OneToMany(targetEntity="Membership", mappedBy="client")
     * @Serializer\Groups({"list"})
     */
    protected $memberships;

    /**
     * @ORM\OneToMany(targetEntity="Access", mappedBy="client")
     * @Serializer\Groups({"list"})
     */
    protected $accesses;


    /**
     * @ORM\Column(type="datetime",nullable=false)
     * @Serializer\Groups({"details"})
     * @Serializer\SerializedName("createdAt")
     */
    private  $createdAt;

    /**
     * @ORM\Column(type="datetime",nullable=false)
     * @Serializer\Groups({"details"})
     * @Serializer\SerializedName("modifiedAt")
     */
    private  $modifiedAt;


    /**
     * Constructor
     */
    public function __construct($state=self::STATE_PENDING)
    {
        $this->memberships = new \Doctrine\Common\Collections\ArrayCollection();
        $this->accesses = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setCreatedAt(new \DateTime());
        $this->setState($state);
    }


    public function __toString(){
        return $this->getName().','.$this->getSurname();
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCardType
     *
     * @param integer $idCardType
     * @return Client
     */
    public function setIdCardType($idCardType)
    {
        $this->idCardType = $idCardType;

        return $this;
    }

    /**
     * Get idCardType
     *
     * @return integer 
     */
    public function getIdCardType()
    {
        return $this->idCardType;
    }

    /**
     * Set idCardNumber
     *
     * @param string $idCardNumber
     * @return Client
     */
    public function setIdCardNumber($idCardNumber)
    {
        $this->idCardNumber = $idCardNumber;

        return $this;
    }

    /**
     * Get idCardNumber
     *
     * @return string 
     */
    public function getIdCardNumber()
    {
        return $this->idCardNumber;
    }


    /**
     * Set cardNumber
     *
     * @param string $cardNumber
     * @return Membership
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    /**
     * Get cardNumber
     *
     * @return string
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }
    /**
     * Set name
     *
     * @param string $name
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return Client
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }


    /**
     * Set state
     *
     * @param string $state
     * @return Client
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }


    /**
     * Add memberships
     *
     * @param \AppBundle\Entity\Membership $memberships
     * @return Client
     */
    public function addMembership(\AppBundle\Entity\Membership $memberships)
    {
        $this->memberships[] = $memberships;

        return $this;
    }

    /**
     * Remove memberships
     *
     * @param \AppBundle\Entity\Membership $memberships
     */
    public function removeMembership(\AppBundle\Entity\Membership $memberships)
    {
        $this->memberships->removeElement($memberships);
    }

    /**
     * Get memberships
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMemberships()
    {
        return $this->memberships;
    }




    /**
     * Add access
     *
     * @param \AppBundle\Entity\Access $access
     * @return Client
     */
    public function addAccess(\AppBundle\Entity\Access $access)
    {
        $this->accesses[] = $access;

        return $this;
    }

    /**
     * Remove access
     *
     * @param \AppBundle\Entity\Access $access
     */
    public function removeAccess(\AppBundle\Entity\Access $access)
    {
        $this->accesses->removeElement($access);
    }

    /**
     * Get access
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAccesses()
    {
        return $this->accesses;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @param mixed $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }



    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateModifiedDatetime() {
        $this->setModifiedAt(new \DateTime());
    }


    public function activate(){
        if($this->getState() == self::STATE_PENDING && $this->cardNumber !==null ){
            $this->setState(1);
        }
    }

    public function  getActiveMembership(){

        $memberShipCollection = $this->getMemberships();

        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("status", 1))
            ->setFirstResult(0);
       $actives =  $memberShipCollection->matching($criteria);
       return $actives[0];

    }
}

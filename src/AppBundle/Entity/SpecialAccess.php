<?php
/**
 * Created by PhpStorm.
 * User: asalfo
 * Date: 04/06/16
 * Time: 20:58
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Validator\Constraints as AppAssert;
/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\SpecialAccessRepository")
 * @ORM\Table(name="special_access")
 */
class SpecialAccess
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected  $id;

    /**
     * @ORM\Column(type="integer")
     * @AppAssert\LockerConstraint
     */
    protected  $locker;
    /**
     * @ORM\Column(type="datetime")
     */
    protected  $startDate;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected  $endDate;


    /**
     * Access constructor.
     */
    public function __construct($locker=null)
    {
        $this->startDate = new \DateTime();
        $this->locker = $locker;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set locker
     *
     * @param integer $locker
     *
     * @return SpecialAccess
     */
    public function setLocker($locker)
    {
        $this->locker = $locker;

        return $this;
    }

    /**
     * Get locker
     *
     * @return integer
     */
    public function getLocker()
    {
        return $this->locker;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return SpecialAccess
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return SpecialAccess
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }


    public function getElapsedTime(){

        $diff = null;
        if($this->getEndDate()){
            $diff = $this->getEndDate()->diff($this->getStartDate());
        }else{
            $now = new \DateTime();
            $diff = $now->diff($this->getStartDate());
        }

        return  $diff;
    }
}

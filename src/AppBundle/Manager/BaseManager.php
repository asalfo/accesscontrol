<?php


namespace AppBundle\Manager;


use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityManager;

abstract class BaseManager
{

    const DATE_RANGE_FILTER = "date";
    const NUMBER_RANGE_FILTER = "number";

    protected $entityManager;
    protected $repository;
    protected $columns = array();

    /**
     * BaseManager constructor.
     * @param EntityManager $entityManager
     * @param $class
     */
    public function __construct(EntityManager $entityManager, $class)
    {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository($class);
    }


    /**
     * @return mixed
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param mixed $columns
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;
    }


    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param \Doctrine\ORM\EntityRepository $repository
     */
    public function setRepository($repository)
    {
        $this->repository = $repository;
    }

    /**
     * Create the data output array for the DataTables rows
     *
     * @param  array $columns Column information array
     * @param  array $data Data from the SQL result
     * @return array          Formatted data in a row based format
     */
    public function getOutputData($columns, $data)
    {

        $out = array();
        for ($i = 0, $ien = count($data); $i < $ien; $i++) {
            $row = array();
            for ($j = 0, $jen = count($columns); $j < $jen; $j++) {
                $column = $columns[$j];

                // Is there a formatter?
                if (isset($column['formatter'])) {
                    if (isset($data[$i][$this->columnName($column['db'])])) {
                        $row[$column['dt']] = $column['formatter']($data[$i][$this->columnName($column['db'])], $data[$i]);
                    } else {
                        $row[$column['dt']] = $column['formatter'](null, $data[$i]);
                    }
                } else {
                    $row[$column['dt']] = $data[$i][$this->columnName($columns[$j]['db'])];
                }
            }
            $out[] = $row;
        }
        return $out;
    }


    /**
     * Paging
     *
     * Construct the LIMIT clause for server-side processing DQL query
     *
     * @param  array $request Data sent to server by DataTables
     * @param  QueryBuilder $queryBuilder
     * @return string SQL limit clause
     */
    public function addLimitQuery($request, QueryBuilder $queryBuilder)
    {
        $offset = $request['start'];
        $length = $request['length'];
        if ($offset && $length) {
            $queryBuilder->setFirstResult($offset)
                ->setMaxResults($length);
        }
        return $queryBuilder;
    }


    /**
     * Ordering
     *
     * Construct the ORDER BY clause for server-side processing DQL query
     *
     * @param  array $request Data sent to server by DataTables
     * @param  array $columns Column information array
     * @return string SQL order by clause
     */
    public function AddOrderQuery($request, $columns, QueryBuilder $queryBuilder)
    {
        if (isset($request['order']) && count($request['order'])) {
            $orderBy = array();
            $dtColumns = $this->pluck($columns, 'dt');
            for ($i = 0, $ien = count($request['order']); $i < $ien; $i++) {
                // Convert the column index into the column data property
                $columnIdx = intval($request['order'][$i]['column']);
                $requestColumn = $request['columns'][$columnIdx];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                if ($requestColumn['orderable'] == 'true') {
                    $dir = $request['order'][$i]['dir'] === 'asc' ?
                        'ASC' :
                        'DESC';
                    $this->addOrderByFields($orderBy, $column['db'], $queryBuilder->getRootAliases(), $dir);
                }
            }

            $queryBuilder->add('orderBy', implode(',', $orderBy));
        }
        return $queryBuilder;
    }

    private function  addOrderByFields(&$orderBy, $column, $queryRootAlias, $dir)
    {
        if (is_array($column)) {
            $name = key($column);
            $col = $column[$name];
            if (!isset($col['alias'])) {
                $col['alias'] = $queryRootAlias[0];
            }
            if (is_array($col['field'])) {
                $orderBy[] = $this->buildOperatorParts($col) . ' ' . $dir;

            } else {
                $orderBy[] = $col['alias'] . '.' . $col['field'] . ' ' . $dir;
            }
        } else {
            $orderBy[] = $queryRootAlias[0] . '.' . $column . ' ' . $dir;
        }

    }


    private function  columnName($column)
    {
        if (is_array($column)) {
            $columnName = key($column);
        } else {
            $columnName = $column;
        }
        return $columnName;
    }

    private function  buildFilterQuery(&$filters, &$params, $column, QueryBuilder $queryBuilder, $searchString)
    {
        $aliases = $queryBuilder->getRootAliases();
        if (is_array($column)) {
            $name = key($column);
            $col = $column[$name];
            if (!isset($col['alias'])) {
                $col['alias'] = $aliases[0];
            }
            if (is_array($col['field'])) {
                $filters[] = $queryBuilder->expr()->like($this->buildOperatorParts($col), ':search_' . $name);
                $params[':search_' . $name] = '%' . $searchString . '%';

            } else {
                $filters[] = $queryBuilder->expr()->like($col['alias'] . '.' . $col['field'], ':search_' . $name);
                $params[':search_' . $name] = '%' . $searchString . '%';
            }

        } else {
            $filters[] = $queryBuilder->expr()->like($aliases[0] . '.' . $column, ':search_' . $column);
            $params[':search_' . $column] = '%' . $searchString . '%';
        }

    }



    private function buildRangeFilter(&$filters,&$params,QueryBuilder $queryBuilder,$filter)
    {
        $aliases = $queryBuilder->getRootAliases();
        if(!empty($filter['min']) && !empty($filter['max'])) {
            if ($filter['type'] == self::DATE_RANGE_FILTER) {
                $field = $aliases[0] . '.' . $filter['field'];
                $filters[] = $queryBuilder->expr()->between($field, ':range_min' . $filter['field'], ':range_max' . $filter['field']);
                $params[':range_min' . $filter['field']] = new \Datetime(str_replace("/","-",$filter['min']).' 00:00:00');
                $params[':range_max' . $filter['field']] = new \Datetime(str_replace("/","-",$filter['max']).' 23:59:59');
            }else {
                $field = $aliases[0] . '.' . $filter['field'];
                $filters[] = $queryBuilder->expr()->between($field, ':range_min' . $filter['field'], ':range_max' . $filter['field']);
                $params[':range_min' . $filter['field']] = $filter['min'];
                $params[':range_max' . $filter['field']] = $filter['max'];
            }
        } elseif(!empty($filter['min'])){
            if ($filter['type'] == self::DATE_RANGE_FILTER) {
                $field = $aliases[0] . '.' . $filter['field'];
                $filters[] = $queryBuilder->expr()->gte($field, ':range_min' . $filter['field']);
                $params[':range_min' . $filter['field']] = new \Datetime(str_replace("/","-",$filter['min']).' 00:00:00');
            }else {
                $field = $aliases[0] . '.' . $filter['field'];
                $filters[] = $queryBuilder->expr()->gte($field, ':range_min' . $filter['field']);
                $params[':range_min' . $filter['field']] = $filter['min'];
            }

        }elseif(!empty($filter['max'])){
            if ($filter['type'] == self::DATE_RANGE_FILTER) {
                $field = $aliases[0] . '.' . $filter['field'];
                $filters[] = $queryBuilder->expr()->lte($field, ':range_max' . $filter['field']);
                $params[':range_max' . $filter['field']] = new \Datetime(str_replace("/","-",$filter['max']).' 23:59:59');
            }else {
                $field = $aliases[0] . '.' . $filter['field'];
                $filters[] = $queryBuilder->expr()->lte($field, ':range_max' . $filter['field']);
                $params[':range_max' . $filter['field']] = $filter['max'];
            }

        }
    }

    /**
     * Searching / Filtering
     *
     * Construct the WHERE clause for server-side processing SQL query.
     *
     * NOTE this does not match the built-in DataTables filtering which does it
     * word by word on any field. It's possible to do here performance on large
     * databases would be very poor
     *
     * @param  array $request Data sent to server by DataTables
     * @param  array $columns Column information array
     * @return string SQL where clause
     */
    public function addFilter($request, $columns, QueryBuilder $queryBuilder)
    {
        $globalSearch = array();
        $columnSearch = array();
        $rangeFilter = array();
        $searchParameters = array();
        $dtColumns = $this->pluck($columns, 'dt');
        $searchString = $request['search']['value'];
        if (isset($request['search']) && $searchString != '') {
            for ($i = 0, $ien = count($request['columns']); $i < $ien; $i++) {
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                if ($requestColumn['searchable'] == 'true') {
                    $this->buildFilterQuery($globalSearch, $searchParameters, $column['db'], $queryBuilder, $searchString);
                }
            }

        }
        // Individual column filtering
        if (isset($request['columns'])) {
            for ($i = 0, $ien = count($request['columns']); $i < $ien; $i++) {
                $requestColumn = $request['columns'][$i];
                $columnIdx = array_search($requestColumn['data'], $dtColumns);
                $column = $columns[$columnIdx];
                $searchString = $requestColumn['search']['value'];
                if ($requestColumn['searchable'] == 'true' && $searchString != '') {
                    $this->buildFilterQuery($columnSearch, $searchParameters, $column['db'], $queryBuilder, $searchString);
                }
            }
        }

        // Ranger filter;
        if (isset($request['rangeFilters'])) {
            for ($i = 0, $ien = count($request['rangeFilters']); $i < $ien; $i++) {
                $filer = $request['rangeFilters'][$i];
                $this->buildRangeFilter($rangeFilter,$searchParameters,$queryBuilder,$filer);
            }
        }

        if (count($globalSearch)) {
            $queryBuilder->where('(' . implode(' OR ', $globalSearch) . ')');
            $queryBuilder->setParameters($searchParameters);
        }
        if (count($columnSearch)) {
            $queryBuilder->where('(' . implode(' AND ', $columnSearch) . ')');
            $queryBuilder->setParameters($searchParameters);
        }

        if (count($rangeFilter)) {
            $queryBuilder->where('(' . implode(' AND ', $rangeFilter) . ')');
            $queryBuilder->setParameters($searchParameters);
        }

        return $queryBuilder;
    }

    private function  addJoins($columns, QueryBuilder $queryBuilder)
    {

        $dbColumns = $this->pluck($columns, 'db');
        $joined = array();
        foreach ($dbColumns as $column) {
            if (is_array($column)) {
                $name = key($column);
                $col = $column[$name];
                $aliases = $queryBuilder->getRootAliases();
                if (isset($col['alias'])) {
                    if (!isset($joined[$col['alias']])) {
                        $queryBuilder->leftJoin($aliases[0] . '.' . $col['db'], $col['alias']);
                        $joined[$col['alias']] = $col['db'];
                    } else if ($joined[$col['alias']] != $col['db']) {
                        $queryBuilder->leftJoin($aliases[0] . '.' . $col['db'], $col['alias']);
                    }
                }
            }
        }
    }

    private function  addSelectedColumns($columns, QueryBuilder $queryBuilder)
    {
        $column = $this->pluck($columns, 'db');
        $selected = array();
        $aliases = $queryBuilder->getRootAliases();
        foreach ($column as $col) {
            if (is_array($col)) {
                $name = key($col);
                $col = $col[$name];
                if (isset($col['field'])) {
                    if (is_array($col['field'])) {
                        if (!isset($col['alias'])) {
                            $col['alias'] = $aliases[0];
                        }
                        $selected[] = $this->buildOperatorParts($col) . ' AS ' . $name;

                    } else {

                        $selected[] = $col['alias'] . '.' . $col['field'] . ' AS ' . $name;
                    }
                }
            } else {
                $selected[] = $aliases[0] . '.' . $col;
            }
        }
        $queryBuilder->select(implode(',', $selected));

    }

    private function  buildOperatorParts($col, $space = "' '")
    {

        $operator = $col['operator'];
        $i = 1;

        $str = str_replace(':space', (string)$space, $operator);
        foreach ($col['field'] as $field) {

            $pos = '%' . $i;
            $field = $col['alias'] . '.' . $field;
            $str = str_replace($pos, $field, $str);
            $i++;
        }

        return $str;

    }

    /**
     * Pull a particular property from each assoc. array in a numeric array,
     * returning and array of the property values from each item.
     *
     * @param  array $a Array to get data from
     * @param  string $prop Property to read
     * @return array        Array of property values
     */
    protected function pluck($a, $prop)
    {
        $out = array();
        for ($i = 0, $len = count($a); $i < $len; $i++) {
            $out[] = $a[$i][$prop];
        }
        return $out;
    }

    /**
     * Return a string from an array or a string
     *
     * @param  array|string $a Array to join
     * @param  string $join Glue for the concatenation
     * @return string Joined string
     */
    protected function _flatten($a, $join = ' AND ')
    {
        if (!$a) {
            return '';
        } else if ($a && is_array($a)) {
            return implode($join, $a);
        }
        return $a;
    }


    /**
     * The difference between this method and the `simple` one, is that you can
     * apply additional `where` conditions to the SQL queries. These can be in
     * one of two forms:
     *
     * * 'Result condition' - This is applied to the result set, but not the
     *   overall paging information query - i.e. it will not effect the number
     *   of records that a user sees they can have access to. This should be
     *   used when you want apply a filtering condition that the user has sent.
     * * 'All condition' - This is applied to all queries that are made and
     *   reduces the number of records that the user can access. This should be
     *   used in conditions where you don't want the user to ever have access to
     *   particular records (for example, restricting by a login id).
     *
     * @param  array $request Data sent to server by DataTables
     * @param  string $primaryKey Primary key of the table
     * @param  array $columns Column information array
     * @param  array $whereResult WHERE condition to apply to the result set
     * @param  array $whereAll WHERE condition to apply to all queries
     * @return array          Server-side processing response array
     */
    public function getComplexData($request, $columns, $whereResult = null, $whereAll = null)
    {
        // Build the SQL query string from the request
        $queryBuilder = $this->getRepository()->createQueryBuilder('p');

        $this->addJoins($columns, $queryBuilder);
        $this->addLimitQuery($request, $queryBuilder);
        $this->addOrderQuery($request, $columns, $queryBuilder);
        $this->addFilter($request, $columns, $queryBuilder);

        if ($whereResult) {
            $queryBuilder->andWhere($whereResult);
        }
        if ($whereAll) {

            foreach($whereAll as $field=>$value){
               $queryBuilder->andWhere('p.'.$field.'= :where_'.$field);
                $queryBuilder->setParameter(':where_'.$field,$value);
            }
        }

        // Data set length after filtering
        $recordsFiltered = $queryBuilder->select("COUNT(p.id)")->getQuery()
            ->setFirstResult(null)
            ->setMaxResults(null)
            ->getSingleScalarResult();


        $this->addSelectedColumns($columns, $queryBuilder);

        $data = $queryBuilder->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

        // Total data set length
        $recordsTotal = $this->getRepository()->totalRecord();

        /*
         * Output
         */
        return array(
            "draw" => isset ($request['draw']) ?
                intval($request['draw']) :
                0,
            "recordsTotal" => intval($recordsTotal),
            "recordsFiltered" => intval($recordsFiltered),
            "data" => $this->getOutputData($columns, $data)
        );
    }


}
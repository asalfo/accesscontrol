<?php
/**
 * Created by PhpStorm.
 * User: salif.guigma
 * Date: 11/2/15
 * Time: 10:02 AM
 */

namespace AppBundle\Manager;

use Doctrine\DBAL\Connection;

class AccessManager extends Manager
{
    /**
     * AccessManager constructor.
     * @param Connection $connection
     * @param $class
     */
    public function __construct(Connection $connection,$class)
    {
        parent::__construct($connection,$class);

        self::$joins = array(
                    array("table"=>"client",
                          "alias"=>"cl",
                          "field"=> "id",
                          "foreign_field"=> "client_id",
                          "type" => "LEFT")
        );

        $this->columns = array(
            array( 'db' => array('user_id'=>array('db'=>'client','alias'=>'cl','field'=>'id')),
                'dt' => 0,
                'formatter' => function( $d, $row ) {
                    return $d;
                }),
            array( 'db' =>array('username'=>array('db'=>'client','alias'=>'cl','field'=>array('name',' ','surname'),'operator'=>'(%1 ||:space || %3)')),
                'dt' => 1,
                'formatter' => function( $d, $row ) {
                    return "<a href='/client/".$row['user_id']."/edit'>".$d."</a>";
                }),
            array( 'db' => 'locker',  'dt' => 2 ),
            array( 'db' => 'start_date',
                    'dt' => 3,
                    'formatter' => function( $d, $row ) {
                        if($d != null) {
                            if(is_string($d)){
                                $date = new \DateTime($d);
                                return $date->format('d-m-Y H:i:s');
                            }else {
                                return $d->format('d-m-Y H:i:s');
                            }
                        }
                        return '';
                    }),
            array( 'db' => 'end_date',
                'dt' => 4,
                'formatter' => function( $d, $row ) {
                    if($d != null) {
                        if(is_string($d)){
                            $date = new \DateTime($d);
                            return $date->format('d-m-Y H:i:s');
                        }else {
                            return $d->format('d-m-Y H:i:s');
                        }
                    }
                    return '';
                }),
             array( 'db' =>array('duration'=>array()),
                'dt' => 5,
                 'formatter' => function( $d, $row ) {
                     $endDate = new \DateTime($row['end_date']);
                     $startDate = new \DateTime($row['start_date']);
                     if( $endDate != null && $startDate != null) {
                         $interval = $endDate->diff($startDate);
                         return $this->formatTime($interval);
                     }
                     return '';
                 })
        );

    }


    private  function formatTime(\DateInterval $duration){
        $formated = $duration->format('%d,%h,%i');
        $atime = explode(',',$formated);
        $days = (($atime[0] > 0) ? ($atime[0] > 1 ? $atime[0].' dias': $atime[0].' dia'): null );
        $hours = (($atime[1] > 0) ? ($atime[1] > 1 ? $atime[1].' horas': $atime[1].' hora'): null );
        $mn= (($atime[0] > 0) ? $atime[2].' mn': null );
        $time = '';
        if($days) $time = $days;
        if($hours) $time.=' '.$hours;
        if($mn)  $time.=' '.$mn;

        return $time;

    }



}
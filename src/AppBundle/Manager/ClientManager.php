<?php
/**
 * Created by PhpStorm.
 * User: salif.guigma
 * Date: 10/27/15
 * Time: 9:12 AM
 */

namespace AppBundle\Manager;


use AppBundle\Entity\Client;
use Doctrine\DBAL\Connection;

class ClientManager extends Manager
{


    /**
     * ClientManager constructor.
     * @param Connection $connection
     * @param $class
     */
    public function __construct(Connection $connection,$class)
    {
        parent::__construct($connection,$class);


        $this->columns = array(
            array( 'db' => 'id_card_number', 'dt' => 0 ),
            array( 'db' => 'card_number',    'dt' => 1 ),
            array( 'db' => 'name',           'dt' => 2 ),
            array( 'db' => 'surname',        'dt' => 3 ),
            array( 'db' => 'created_at',
                'dt' => 4,
                'formatter' => function( $d, $row ) {

                    if($d != null) {
                        if(is_string($d)){
                            $date = new \DateTime($d);
                            return $date->format('d-m-Y');
                        }else {
                            return $d->format('d-m-Y');
                        }
                    }
                    return '';
                }),
            array( 'db' => 'id',
                'dt' => 5 ,
                'formatter' => function( $d, $row ) {
                    return "<a class='btn btn-primary' href='/client/".$d."/edit'>
                               <i class='fa fa-edit'></i>Modificar</a>";
                })
        );
    }
    
    
    public function addEspecialClient(){

        $client = new $this->class();
        $client = new Client();
        $client->setCardNumber(time());
        $client->setIdCardType(Client::PASSPORT);
        $client->setIdCardNumber('ESP'.time());
        $client->setState(Client::STATE_ACTIVE);

        
    }
}
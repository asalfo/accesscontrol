/*!
 * Assets for Access Control
 */





    function normalizeFormFieldName(string){
    var re = /\[(.*?)\]/g;
    var m;
    if ((m = re.exec(string)) !== null) {
        if (m.index === re.lastIndex) {
            re.lastIndex++;
        }
        return m[1];
    }
    return string;
}

    function trimNumber(s) {
        while (s.substr(0,1) == '0' && s.length>1) { s = s.substr(1,9999); }
        return s;
    }




function DateDiff(date1,date2){
    console.log(date1);
    var date = new Date(date1.replace(/([0-9]+)\/([0-9]+)/,'$2/$1'));
    var date1 = new Date(date2.replace(/([0-9]+)\/([0-9]+)/,'$2/$1'));
    var diff = new Date(Date.parse(date) - Date.parse(date1))
    if (!isNaN(diff.getTime())) {

            return diff;

    }
}


// Function for converting a dd/mm/yyyy date value into a numeric string for comparison (example 08/12/2010 becomes 20100812
function parseDateValue(rawDate) {
    var dateArray= rawDate.split("/");
    var parsedDate= dateArray[2] + dateArray[1] + dateArray[0];
    console.log(parsedDate);
    return parsedDate;
}



function insertSuccessMessage(element,message){

    var html = '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>';
    html+= '<span class="sr-only">Success:</span>';
    html += message;
    element.html(html);
    element.removeClass("hidden");
    element.removeClass("alert-danger");
    element.addClass("alert-success");
    console.log(message);
}

function insertErrorMessage(element,message){

    var html = '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>';
    html+= '<span class="sr-only">Error:</span>';
    html += message.msg;
    if(message.errors){
        html += " ( "+message.errors+" )"
    }

    element.html(html);
    element.removeClass("hidden");
    element.removeClass("alert-success");
    element.addClass("alert-danger");
}

function assignLocker(cardNumberInput,lockerInput,modeInput){
    cardNumber = trimNumber($("#"+cardNumberInput).val());
    locker = $("#"+lockerInput).val();
    mode = $("#"+modeInput).val();
    var url = "/api/v1/locker/assign";
    $.post(url,{"data":{'cardNumber': cardNumber,'locker':locker,'mode':mode  }}, function (data) {
      var response = data
    },"json").done(function(response) {
        if(response.status == 200) {
            insertSuccessMessage($("#status-message"),response.msg);
            $("#access_locker").parent().toggleClass('has-error');
        }else{
            $("#"+lockerInput).parent().addClass('has-error');
            insertErrorMessage($("#status-message"),response);
        }
    }).fail(function(data) {
        $("#lockerNumber").parent().toggleClass('has-error');
    });
}





$(function() {

    $('#side-menu').metisMenu();

    //Loads the correct sidebar on window load,
    //collapses the sidebar on window resize.
    // Sets the min-height of #page-wrapper to window size
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }

    $('#appDataTables').DataTable({
        responsive: true
    });

  $('#clientDataTables').DataTable( {
      responsive:true,
      "dom": 'Bfrtip',
      buttons: [
          "pageLength",
          {
              extend: 'csv',
              title: 'Socios',
              message:'Listado de socios',
              exportOptions: {
                  columns:[1,2,3,4,5]
              }
          },
          {
              extend: 'pdf',
              text: 'PDF',
              title: 'Socios',
              message:'Listado de socios',
              exportOptions: {
                  columns:[0,1,2,3,4]
              }
          }
      ],
        "columnDefs": [
            {
                "searchable": false,
                "orderable": false,
                "targets": 5
            },

        ],
        "language": {
            "url": "/Spanish.json",
            buttons: {
                pageLength: {
                    "_": "Mostar %d elementos",
                    "-1": "Mostrar todo"
                }
            }
        },
        "lengthMenu": [[50,100, 200, -1], [50,100, 200, "Todos"]],
        "processing": true,
        "serverSide": true,
        "ajax":{
            "url":"/client/json",
            "type": "POST"
        },
        "deferLoading": 200
    } );


    var accesTable = $('#accessDataTables').DataTable({
        responsive:true,
        "dom": 'Bfrtip',
        buttons: [
            "pageLength",
            {
                extend: 'csv',
                title: 'Socios',
                message:'Listado de socios',
                exportOptions: {
                    columns:[1,2,3,4,5]
                }
            },
            {
                extend: 'pdf',
                text: 'PDF',
                title: 'Socios',
                message:'Listado de socios',
                exportOptions: {
                    columns:[1,2,3,4,5]
                }
            }
        ],
        "columnDefs": [
            {
                "searchable": false,
                "orderable": false,
                "targets": 5
            },
            {"visible": false, "targets": [0]}
        ],
        "language": {
            "url": "/Spanish.json",
            buttons: {
                pageLength: {
                    "_": "Mostar %d elementos",
                    "-1": "Mostrar todo"
                }
            }
        },
        "lengthMenu": [[50, 100, 200, -1], [50, 100, 200, "Todos"]],
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: "/access/json",
            "type": "POST",
            "data": function (data) {
                data.rangeFilters = [{
                    type: 'date',
                    field: 'startDate',
                    min: $('#startDate').val(),
                    max: $('#endDate').val()
                }];
            }
        }
    });

    $('#startDate').change(function () {
        accesTable.draw();
    });
    $('#endDate').change(function () {
        accesTable.draw();
    });
    $('#filter_reset').click(function(){
        setTimeout(function()
        {
            accesTable.draw();
        }, 1000);

    })


    $('#appDataTables').DataTable({
        responsive: true
    });

});
